��    d      <  �   \      �     �     �     �     �     �     �     �  	   		  
   	     	  
   /	     :	     K	     W	     o	     �	     �	     �	     �	  (   �	  )   �	     
     
     
  	   
  
   '
     2
     :
  5   L
  
   �
  )   �
     �
     �
  e   �
  a   3  -   �  	   �  
   �  +   �       
          
        (  B   B      �     �     �     �     �     �     �     �  
   �     �     	               #     4     J     f     o     u     }     �     �  !   �     �     �     �     �  
   �             ?     q   M  G   �  N        V  	   c     m     t     |     �     �  .   �  )   �  :     :   I  #   �  '   �     �     �     �       K     	   j     t  @  y     �     �     �               &     2  	   K     U     f  
   w     �     �     �     �     �     �  	   �     �  -     3   <     p     w     ~  
   �     �     �     �  =   �     �  ,   �     )     :  `   ?  ^   �  7   �  	   7  
   A  /   L     |  	   �     �  
   �     �  G   �  !   �       
   -     8     H     X     ]     j     s     �     �     �     �     �     �     �     �     �          
            1   '     Y  !   j     �     �     �     �     �  O   �  a     ?   q  ?   �     �  
                        /     J  =   e  -   �  G   �  ?     *   Y  8   �     �     �     �     �  Q   �     C     H     b   !   S   <           @      *   a   =      c       L         A   O   ;   8       U   ?       "                 V                 H      T      W       F          &             `          d       ]   (   Q   4   	   :       K   \   '   $   %   5      B   .   M                         I       2                  N   6   _       #          [                    +   X       
   7      Z   Y   0   R          ^          3   9   E       D              J   G       ,   1          )      -   >           P   C   /        Account activation failed Account activation on Activate account at Additional info Address line 1 Address line 2 Aditional information Argentina Arrival at Arrival location Atractions Awaiting payment Back to top Book the package below. Book this package Book your visit Brazil Canceled Checkout our packages. Checkout the atractions in the tripoint. Choose hotel and tours. Price on request. City Close Closed Confirmed Contact Us Country Customize package Email with password reset instructions has been sent. First name For %(count)d person For %(count)d people Forgot password Hello I have read and agree to the <a data-toggle="modal" data-target="#tac-modal">Terms and Conditions</a> I have read and agree to the <a data-toggle="modal" data-target="#tos-modal">Terms of Service</a> If needed, write additional information here. Last name Learn more Link is valid for %(expiration_days)s days. Log in Logged out Logout MM/DD/YYYY Make Your Custom Package! Make your own package, choose hotel and tours and get a quotation. Maximum length of 30 characters. Minimum of 2 people. Misc My Bookings My bookings New New booking Next Not member Number of people Package Packages Paraguay Password changed Password reset failed Password reset successfully Previous Price Prices: Private Register Registration Registration is currently closed. Reset it Reset password at %(site_name)s Shared Sign in Start date State Submit Thank you for booking with us, this is your booking information The package has been booked, a email was sent to you with more details, you can follow the status of your booking The package has been booked, a email was sent to you with more details. This email address is already in use. Please supply a different email address. Travel Guide Utilities Viewed Welcome What's included What's not included What's not included: You are now registered. Activation email sent. You can follow the status of your booking You can follow the status of your package in MyBookings at You haven't booked any package yet. Check out our packages You must agree to the terms to book You must agree to the terms to register Your Bookings. Your account is now activated. Your email address Your message Your message has been sent succesfully, we will respond as fast as possible Your name here Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-17 14:19+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Falha na ativação de conta Ativação de conta em Ative sua conta em Informações adicionais Endereço 1 Endereço 2 Informações adicionais Argentina Local de chegada Local de chegada Atrações Aguardando pagamento Voltar ao topo Agende o pacote abaixo. Reservar este pacote Agende sua visita Brasil Cancelado Confira nossos pacotes. Confira as atrações da tríplice fronteira. Escolha o hotel e os passeios. Preço sob consulta. Cidade Fechar Fechado Confirmado Contato País Modificar pacote Um email com instruções para resetar sua senha foi enviado. Primeiro Nome Para %(count)d pessoa Para %(count)d pessoas Esqueceu a senha Olá Eu li e concordo com os <a data-toggle="modal" data-target="#tac-modal">Termos e Condições</a> Eu li e concordo com os <a data-toggle="modal" data-target="#tos-modal">Termos de Serviço</a> Se necessário, adicione informações adicionais aqui. Sobrenome Saiba mais O link é válido por %(expiration_days)s days. Entre Deslogado Sair DD/MM/AAAA Crie seu pacote! Crie seu pacote, escolha o hotel e os passeios, e receba uma cotação. Tamanho máximo de 30 caracteres. Mínimo de 2 pessoas. Variedades Minhas Reservas Minhas reservas Novo Nova reserva Próximo Não é membro Número de pessoas Pacote Pacotes Paraguay Senha modificada Falha ao resetar senha Senha resetada com sucesso Anterior Preço: Preços: Privado Registre-se Cadastro O registro de novos usuários está indispinível Resete sua senha Resete sua senha em %(site_name)s Compartilhado Entrar Data de início Estado Enviar Obrigado por reservar seu pacote conosco, segue as informações da sua reserva O pacote foi reservado, te enviamos um email com mais detalhes, você pode acompanhar sua reserva O pacote foi reservado, te enviamos um email com mais detalhes. Este email já está em uso. Por favor providencie outro email. Guia de Viagem Utilidades Visto Bemvindo O que está incluido O que não está incluido: O que não está incluido: Você está registrado. Um email para ativação foi enviado. Você pode acompanhar o status da sua reserva Você pode acompanhar o status do seu pacote no menu Minhas reservas em Você ainda não reservou nenhum pacote. Confira nossos pacotes Você deve aceitar os termos para reservar Você deve aceitar o termo de serviço para se registrar Suas Reservas. Sua conta foi ativada Email Mensagem Sua mensagem foi enviada com sucesso, nós responderemos o mais rápido possível Nome aqui 