from django.utils.translation import ugettext_lazy as _
from django import forms
from django.contrib.auth.forms import UserCreationForm

from registration.users import UserModel, UsernameField

from bookings.models import Country


User = UserModel()

class RegisterForm(UserCreationForm):
    required_css_class = 'required'
    email = forms.EmailField(label=("E-mail"))
    username = forms.CharField(max_length=30, required=False, widget=forms.HiddenInput())
    first_name = forms.CharField(max_length=30, required=True, 
        label=_('First name'))
    last_name = forms.CharField(max_length=30, required=True,
        label=_('Last name'))
    address_line_1 = forms.CharField(max_length=100, required=True,
        label=_('Address line 1'))
    address_line_2 = forms.CharField(max_length=100, required=False,
        label=_('Address line 2'))
    city = forms.CharField(max_length=100, required=True,
        label=_('City'))
    state = forms.CharField(max_length=100, required=True,
        label=_('State'))
    country = forms.ModelChoiceField(
            queryset = Country.objects.all(),
            required = True,
            empty_label=None,
            label=_('Country'))

    tos = forms.BooleanField(widget=forms.CheckboxInput,
                             label = _('I have read and agree to the <a data-toggle="modal" data-target="#tos-modal">Terms of Service</a>'),
                             error_messages = {'required': _("You must agree to the terms to register")})

    class Meta:
        model = User
        fields = (UsernameField(), 'first_name', 'last_name','email')

    def clean_email(self):
        email = self.cleaned_data['email']
        if len(email) > 30:
            raise forms.ValidationError('Maximum length of 30 characters.')
        self.cleaned_data['username'] = email
        if User.objects.filter(email__iexact=email):
            raise forms.ValidationError(_('This email address is already in use. Please supply a different email address.'))
        return email

    def clean_first_name(self):
        fname = self.cleaned_data['first_name']
        if len(fname) > 30:
            raise forms.ValidationError(_('Maximum length of 30 characters.'))
        return fname

    def clean_last_name(self):
        lname = self.cleaned_data['last_name']
        if len(lname) > 30:
            raise forms.ValidationError(_('Maximum length of 30 characters.'))
        return lname
