from django.contrib import admin
from django.db import models
from django.forms import CheckboxSelectMultiple

from modeltranslation.admin import TranslationAdmin

from .models import HomeCarouselImage
from .models import Atraction
from .models import PostCategory
from .models import Post


class HomeCarouselImageAdmin(TranslationAdmin):
    list_display = ('title', 'admin_thumbnail', 'published')
    list_filter = ('published',)
    search_fields = ['title']

    fieldsets = [
        ('Content information', {'fields': ['published']}),
        ('Image information', {'fields': ['title', 'image', 'url']})
    ]


class AtractionAdmin(TranslationAdmin):
    list_display = ('name','country', 'published')
    list_filter = ('country', )
    search_fields = ['name', 'description']
    fieldsets = [
        ('Content information', {'fields': ['published']}),
        ('Atraction information', {'fields': ['country', 'name', 'description']}),
    ]


class PostAdmin(TranslationAdmin):
    list_display = ('title', 'category', 'published')
    list_filter = ('category',)
    search_fields = ['title', 'text']
    fieldsets = [
        ('Content information', {'fields': ['category', 'published']}),
        ('Post information', {'fields': ['title', 'text']}),
    ]


admin.site.register(HomeCarouselImage, HomeCarouselImageAdmin)
admin.site.register(Atraction, AtractionAdmin)
admin.site.register(PostCategory)
admin.site.register(Post, PostAdmin)

