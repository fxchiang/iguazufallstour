# encoding: utf-8

from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse


TOURS_COUNTRIES = (
    ('AR', 'Argentina'),
    ('BR', 'Brazil'),
    ('PY', 'Paraguay'),
)


class HomeCarouselImage(models.Model):
    title = models.CharField(max_length=255)
    image = models.URLField()
    url = models.URLField(max_length=100, null=True, blank=True)
    published = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def admin_thumbnail(self):
        return u'<img src="%s" height=100/>' % (self.image)
    admin_thumbnail.short_description = 'Preview'
    admin_thumbnail.allow_tags = True


class Language(models.Model):
    name = models.CharField(max_length=2)

    def __str__(self):
        return self.name


class Atraction(models.Model):
    country = models.CharField(max_length=2, choices=TOURS_COUNTRIES, default='AR')
    published = models.BooleanField(default=False)
    name = models.CharField(max_length=255)
    description = HTMLField()

    def __str__(self):
        return self.name


class PostCategory(models.Model):
    name = models.CharField(max_length=255)

    class Meta():
        verbose_name_plural = 'Post categories'

    def __str__(self):
        return self.name


class Post(models.Model):
    category = models.ForeignKey(PostCategory, default=1)
    published = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    text = HTMLField()

    def __str__(self):
        return self.title

    def get_absolut_url(self):
        return "/post/%s" %(self.id)