from modeltranslation.translator import register, TranslationOptions

from .models import Atraction
from .models import Post
from .models import HomeCarouselImage


@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)


@register(Atraction)
class AtractionTranslationOptions(TranslationOptions):
	fields = ('name', 'description')


@register(HomeCarouselImage)
class HomeCarouselImageTranslationOptions(TranslationOptions):
	fields = ('title', 'image')