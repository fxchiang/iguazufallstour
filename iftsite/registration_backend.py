from django.shortcuts import get_object_or_404
from django.shortcuts import render
from registration.backends.default.views import RegistrationView
from iftsite.forms import RegisterForm

from bookings.models import UserProfile
from iftsite.models import Post



class MyRegistrationView(RegistrationView):

    form_class = RegisterForm

    def register(self, request, form_class):
        new_user = super(MyRegistrationView, self).register(request, form_class)
        user_profile = UserProfile()
        user_profile.user = new_user
        user_profile.address_line_1 = form_class.cleaned_data['address_line_1']
        user_profile.address_line_2 = form_class.cleaned_data['address_line_2']
        user_profile.city = form_class.cleaned_data['city']
        user_profile.state = form_class.cleaned_data['state']
        user_profile.country = form_class.cleaned_data['country']
        user_profile.save()

        return user_profile

