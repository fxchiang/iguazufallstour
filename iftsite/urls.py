from django.conf.urls import include, url

from iftsite.registration_backend import MyRegistrationView

from . import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^atractions/$', views.atractions, name='atractions'),
    url(r'^post/(?P<id>[0-9]+)/$', views.post_detail, name='detail'),
    url(r'^auth/register/$', MyRegistrationView.as_view(), name='registration_register'),
]
