# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iftsite', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image',
            field=models.ImageField(upload_to='images/'),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_en',
            field=models.ImageField(null=True, upload_to='images/'),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_pt_br',
            field=models.ImageField(null=True, upload_to='images/'),
        ),
    ]
