# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iftsite', '0003_auto_20160224_1933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image',
            field=models.URLField(),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_en',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_pt_br',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='url',
            field=models.URLField(null=True, blank=True, max_length=100),
        ),
    ]
