# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iftsite', '0004_auto_20160224_1935'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Image',
        ),
    ]
