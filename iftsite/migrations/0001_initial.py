# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Atraction',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('country', models.CharField(max_length=2, choices=[('AR', 'Argentina'), ('BR', 'Brazil'), ('PY', 'Paraguay')], default='AR')),
                ('published', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('name_en', models.CharField(max_length=255, null=True)),
                ('name_pt_br', models.CharField(max_length=255, null=True)),
                ('description', tinymce.models.HTMLField()),
                ('description_en', tinymce.models.HTMLField(null=True)),
                ('description_pt_br', tinymce.models.HTMLField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HomeCarouselImage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('title_en', models.CharField(max_length=255, null=True)),
                ('title_pt_br', models.CharField(max_length=255, null=True)),
                ('image', models.ImageField(upload_to='home_carousel_images/')),
                ('image_en', models.ImageField(upload_to='home_carousel_images/', null=True)),
                ('image_pt_br', models.ImageField(upload_to='home_carousel_images/', null=True)),
                ('url', models.CharField(blank=True, max_length=100, null=True)),
                ('published', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('caption', models.TextField()),
                ('image', models.ImageField(upload_to='images/')),
            ],
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('published', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=255)),
                ('title_en', models.CharField(max_length=255, null=True)),
                ('title_pt_br', models.CharField(max_length=255, null=True)),
                ('text', tinymce.models.HTMLField()),
                ('text_en', tinymce.models.HTMLField(null=True)),
                ('text_pt_br', tinymce.models.HTMLField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PostCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Post categories',
            },
        ),
        migrations.AddField(
            model_name='post',
            name='category',
            field=models.ForeignKey(to='iftsite.PostCategory', default=1),
        ),
    ]
