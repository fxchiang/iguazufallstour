# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iftsite', '0002_auto_20160224_1931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image',
            field=models.ImageField(upload_to=''),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_en',
            field=models.ImageField(upload_to='', null=True),
        ),
        migrations.AlterField(
            model_name='homecarouselimage',
            name='image_pt_br',
            field=models.ImageField(upload_to='', null=True),
        ),
    ]
