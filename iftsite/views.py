from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import HomeCarouselImage
from .models import Atraction
from .models import Post

# from .forms import RegisterForm

# Create your views here.
def home(request):
    carousel_slides = HomeCarouselImage.objects.filter(published=True)
    ar_atractions = Atraction.objects.filter(country='AR', published=True)
    br_atractions = Atraction.objects.filter(country='BR', published=True)
    py_atractions = Atraction.objects.filter(country='PY', published=True)

    context = {
        'carousel_slides': carousel_slides,
        'carousel_indicators': range(len(carousel_slides)),
        'ar_atractions': ar_atractions,
        'br_atractions': br_atractions,
        'py_atractions': py_atractions,
    }

    return render(request, 'iftsite/home.html', context)


def atractions(request):
    ar_atractions = Atraction.objects.filter(country='AR', published=True)
    br_atractions = Atraction.objects.filter(country='BR', published=True)
    py_atractions = Atraction.objects.filter(country='PY', published=True)

    context = {
        'ar_atractions': ar_atractions,
        'br_atractions': br_atractions,
        'py_atractions': py_atractions,
    }

    return render(request, 'iftsite/atractions.html', context)


def post_detail(request, id):
    post = get_object_or_404(Post, pk=id)

    context = {
        'post': post,
    }

    return render(request, 'iftsite/post.html', context)
