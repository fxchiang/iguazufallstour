from .models import Post

def base_template_processor(request):
    guides = Post.objects.filter(category__name="Guide", published=True)
    utilities = Post.objects.filter(category__name='Utilities', published=True)
    miscs = Post.objects.filter(category__name='Misc', published=True)
    # terms of service
    tos = None
    # terms and conditions
    tac = None

    try:
        tos = Post.objects.get(title='Terms of Service')
    except Post.DoesNotExist:
        pass

    try:
        tac = Post.objects.get(title='Terms and Conditions')
    except Post.DoesNotExist:
        pass

    context = {
        'guides': guides,
        'utilities': utilities,
        'miscs': miscs,
        'tos': tos,
        'tac': tac,
    }

    return context
