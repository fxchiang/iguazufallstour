from datetime import date
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext
from django.conf import settings
from django import forms
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.template import RequestContext, loader
from django.contrib.sites.models import Site
from django.db.models import Q
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Hidden
from crispy_forms.layout import Layout
from crispy_forms.layout import ButtonHolder
from crispy_forms.layout import Fieldset
from crispy_forms.layout import HTML
from crispy_forms.layout import Field
from crispy_forms.layout import Div
from crispy_forms.bootstrap import StrictButton

from .models import ItemPrice
from .models import Booking
from .models import UserProfile
from .models import Package
from .models import ItemMode
from .models import ArrivalLocation


class PackageModelForm(forms.ModelForm):

    class Meta:
        model = Package
        fields = ('published', 'name', 'description', 'items', 'profit_percent',
            'mode', 'package_type')

    def clean_items(self):
        items = self.cleaned_data.get('items')
        hotel = items.filter(Q(item__category='Hotel'))

        for item in items:
            if items.filter(Q(item=item.item)).count() > 1:
                raise ValidationError(
                    'A package cannot contain a shared and a private instance of same item')

        if hotel.count() > 1:
            raise ValidationError('A package can contain only one hotel')

        return items


class ItemModeInlineFormset(forms.models.BaseInlineFormSet):

    def clean(self):
        item_modes = []
        for form in self.forms:
            item_modes.append(form.cleaned_data.get('mode'))
            if len(item_modes) != len(set(item_modes)):
                raise ValidationError('Duplicated mode')


class BookPackageForm(forms.ModelForm):

    class Meta:
        model = Booking
        fields = [
            'user',
            'package',
            'booking_date',
            'number_of_people',
            'start_date',
            'arrival_location',
            'additional_info',
        ]

    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [mail_tuple[1] for mail_tuple in settings.MANAGERS]
    email_admin_subject_template = "bookings/email_admin_new_booking_subject.txt"
    email_admin_plain_template = 'bookings/email_admin_new_booking.txt'
    email_admin_html_template = 'bookings/email_admin_new_booking.html'
    email_user_subject_template = "bookings/email_user_new_booking_subject.txt"
    email_user_plain_template = 'bookings/email_user_new_booking.txt'
    email_user_html_template = 'bookings/email_user_new_booking.html'
    # this booking instance
    instance = None

    def __init__(self, *args, **kwargs):
        package = kwargs.pop('package')
        user = kwargs.pop('user')
        self.request = kwargs.pop('request')

        super(BookPackageForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(

            Div(
                Fieldset(
                    '',
                    Field('user', type='hidden'),
                    Field('package', type='hidden'),
                    Field('booking_date', type='hidden'),
                    Field('start_date', css_class='datepicker',
                        placeholder=_('MM/DD/YYYY'), required=True),
                    Field('number_of_people'),
                    Field('arrival_location'),
                    Field('additional_info'),
                    Field('tac'),
                ),
                css_class='col-md-12',
            ),
            HTML('<br>'),
            ButtonHolder(
                Submit('submit', _('Submit'), css_class='btn-success'),
                HTML('<a href={% url "bookings:customize-package" package.pk %} class="btn btn-default">' + ugettext("Customize package")+ '</a>')
            )
        )

        # default value for hidden fields
        self.fields['user'].initial = user.pk
        self.fields['package'].initial = package.pk
        self.fields['booking_date'].initial = date.today()
        # choices filters
        self.fields['number_of_people'] = forms.ChoiceField(
            choices=[ (price, package.get_all_prices()[price])
                for price in package.get_all_prices()],
            required=True,
        )
        self.fields['arrival_location'] = forms.ModelChoiceField(
            queryset=ArrivalLocation.objects.all(),
            required=True,
            empty_label=None,
        )
        # fields labels
        self.fields['start_date'].label = _('Start date')
        self.fields['number_of_people'].label = _('Number of people')
        self.fields['arrival_location'].label = _('Arrival location')
        self.fields['additional_info'].label = _('Additional info')
        # fields help text
        self.fields['additional_info'].help_text = _('If needed, write additional information here.')

    # terms and contidions
    tac = forms.BooleanField(widget=forms.CheckboxInput,
                         label = _('I have read and agree to the <a data-toggle="modal" data-target="#tac-modal">Terms and Conditions</a>'),
                         error_messages = {'required': _("You must agree to the terms to book")})



    def admin_message(self):
        """
        Render the body of the message to a string.
        """
        return loader.render_to_string(self.email_admin_plain_template,
                                       self.get_context())

    def user_message(self):
        return loader.render_to_string(self.email_user_plain_template,
                                       self.get_context())

    def admin_html_message(self):
        """
        Render the body of the message to a string from html.
        """
        return loader.render_to_string(self.email_admin_html_template,
                                       self.get_context())
    def user_html_message(self):
        return loader.render_to_string(self.email_user_html_template,
                                       self.get_context())

    def admin_subject(self):
        """
        Render the subject of the message to a string.
        """
        subject = loader.render_to_string(self.email_admin_subject_template,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def user_subject(self):
        subject = loader.render_to_string(self.email_user_subject_template,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def get_context(self):
        """
        Return the context used to render the templates for the email
        subject and body.
        By default, this context includes:
        * All of the validated values in the form, as variables of the
          same names as their fields.
        * The current ``Site`` object, as the variable ``site``.
        * Any additional variables added by context processors (this
          will be a ``RequestContext``).
        """
        if not self.is_valid():
            raise ValueError(
                "Booking error"
            )
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)
        return RequestContext(self.request,
                              dict(self.cleaned_data,
                                   site=site, instance=self.instance))

    def get_admin_message_dict(self):
        """
        Generate the various parts of the message and return them in a
        dictionary, suitable for passing directly as keyword arguments
        to ``django.core.mail.send_mail()``.
        By default, the following values are returned:
        * ``from_email``
        * ``message``
        * ``recipient_list``
        * ``subject``
        """
        if not self.is_valid():
            raise ValueError(
                "Error"
            )
        message_dict = {}
        message_dict['from_email'] = self.from_email
        message_dict['message'] = self.admin_message()
        message_dict['html_message'] = self.admin_html_message()
        message_dict['recipient_list'] = self.recipient_list
        message_dict['subject'] = self.admin_subject()
        return message_dict

    def get_user_message_dict(self):
        if not self.is_valid():
            raise ValueError(
                "Error"
            )
        message_dict = {}
        message_dict['from_email'] = self.from_email
        message_dict['message'] = self.user_message()
        message_dict['html_message'] = self.user_html_message()
        message_dict['subject'] = self.user_subject()
        return message_dict

    def save(self, commit=True, fail_silently=False):
        # save instance to db
        self.instance = super(BookPackageForm, self).save(commit=True)
        # send email to admin
        send_mail(fail_silently=fail_silently, **self.get_admin_message_dict())
        # send mail to user
        send_mail(fail_silently=fail_silently,
            recipient_list=[self.instance.user.email],
            **self.get_user_message_dict())
        return self.instance


class MakePackageForm(forms.ModelForm):

    class Meta:
        model = Package
        fields = ['items', 'name_en', 'name_pt_br', 'package_type']

    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [mail_tuple[1] for mail_tuple in settings.MANAGERS]
    email_admin_subject_template = "bookings/email_admin_new_booking_subject.txt"
    email_admin_plain_template = 'bookings/email_admin_new_booking.txt'
    email_admin_html_template = 'bookings/email_admin_new_booking.html'
    email_user_subject_template = "bookings/email_user_new_booking_subject.txt"
    email_user_plain_template = 'bookings/email_user_new_booking.txt'
    email_user_html_template = 'bookings/email_user_new_booking.html'
    package_instance = None
    booking_instance = None

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.items = kwargs.pop('items')
        super(MakePackageForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name_en', type='hidden'),
            Field('name_pt_br', type='hidden'),
            Field('items'),
            HTML('<hr>'),
            Field('package_type', type='hidden'),
            Field('booking_date', type='hidden'),
            Field('start_date', css_class='datepicker',
                placeholder=_('MM/DD/YYYY'), required=True),
            Field('number_of_people'),
            Field('arrival_location'),
            Field('additional_info'),
            Field('tac'),
            ButtonHolder(
                Submit('submit', _('Submit'), css_class='btn-success'))
        )

        # default value for hidden fields
        self.fields['name_en'].initial = 'Custom package'
        self.fields['name_pt_br'].initial = 'Pacote personalizado'
        if self.items:
            self.fields['items'].initial = self.items
        self.fields['package_type'].initial = 'custom'
        self.fields['booking_date'].initial = date.today()
        # help texts for fields
        self.fields['number_of_people'].help_text = _('Minimum of 2 people.')
        self.fields['additional_info'].help_text = _('If needed, write additional information here.')
        # fields names
        self.fields['start_date'].label = _('Start date')
        self.fields['number_of_people'].label = _('Number of people')
        self.fields['arrival_location'].label = _('Arrival location')
        self.fields['additional_info'].label = _('Additional info')

    # terms and contidions
    tac = forms.BooleanField(widget=forms.CheckboxInput,
                         label = _('I have read and agree to the <a data-toggle="modal" data-target="#tac-modal">Terms and Conditions</a>'),
                         error_messages = {'required': _("You must agree to the terms to book")})

    items = forms.ModelMultipleChoiceField(
        queryset=ItemMode.objects.filter(
            Q(item__category='Tour')|
            Q(item__category='Extra')|
            Q(item__category='Hotel')),
        widget=forms.CheckboxSelectMultiple)

    number_of_people = forms.IntegerField(max_value=10, min_value=2)

    arrival_location = forms.ModelChoiceField(
        queryset=ArrivalLocation.objects.all(),
        required=True)

    start_date = forms.DateField(widget=forms.DateInput(), required=True)
    booking_date = forms.DateField(widget=forms.DateInput())

    additional_info = forms.CharField(widget=forms.Textarea, required=False)

    def clean_items(self):
        items = self.cleaned_data.get('items')
        hotel = items.filter(Q(item__category='Hotel'))

        for item in items:
            if items.filter(Q(item=item.item)).count() > 1:
                raise ValidationError(
                    'You cannot chose a private and shared instance of the same item')

        if hotel.count() > 1:
            raise ValidationError('Please select only one hotel')

        return items

    def admin_message(self):
        """
        Render the body of the message to a string.
        """
        return loader.render_to_string(self.email_admin_plain_template,
                                       self.get_context())

    def user_message(self):
        return loader.render_to_string(self.email_user_plain_template,
                                       self.get_context())

    def admin_html_message(self):
        """
        Render the body of the message to a string from html.
        """
        return loader.render_to_string(self.email_admin_html_template,
                                       self.get_context())
    def user_html_message(self):
        return loader.render_to_string(self.email_user_html_template,
                                       self.get_context())

    def admin_subject(self):
        """
        Render the subject of the message to a string.
        """
        subject = loader.render_to_string(self.email_admin_subject_template,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def user_subject(self):
        subject = loader.render_to_string(self.email_user_subject_template,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def get_context(self):
        """
        Return the context used to render the templates for the email
        subject and body.
        By default, this context includes:
        * All of the validated values in the form, as variables of the
          same names as their fields.
        * The current ``Site`` object, as the variable ``site``.
        * Any additional variables added by context processors (this
          will be a ``RequestContext``).
        """
        if not self.is_valid():
            raise ValueError(
                "Booking error"
            )
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)
        return RequestContext(self.request,
                              dict(self.cleaned_data,
                                   site=site,
                                   instance=self.booking_instance,
                                   package=self.package_instance))

    def get_admin_message_dict(self):
        """
        Generate the various parts of the message and return them in a
        dictionary, suitable for passing directly as keyword arguments
        to ``django.core.mail.send_mail()``.
        By default, the following values are returned:
        * ``from_email``
        * ``message``
        * ``recipient_list``
        * ``subject``
        """
        if not self.is_valid():
            raise ValueError(
                "Error"
            )
        message_dict = {}
        message_dict['from_email'] = self.from_email
        message_dict['message'] = self.admin_message()
        message_dict['html_message'] = self.admin_html_message()
        message_dict['recipient_list'] = self.recipient_list
        message_dict['subject'] = self.admin_subject()
        return message_dict

    def get_user_message_dict(self):
        if not self.is_valid():
            raise ValueError(
                "Error"
            )
        message_dict = {}
        message_dict['from_email'] = self.from_email
        message_dict['message'] = self.user_message()
        message_dict['html_message'] = self.user_html_message()
        message_dict['subject'] = self.user_subject()
        return message_dict

    def save(self, commit=True, fail_silently=False):
        # print(self.instance)
        # if self.instance:
        #     # create new instance of package
        #     self.package_instance = Package(name=_('Custom package'),
        #         package_type='custom')
        #     self.package_instance.save()
        #     self.package_instance.items = self.cleaned_data.get('items')
        #     self.package_instance.save()
        # else:
        self.package_instance = super(MakePackageForm, self).save(commit=True)

        self.booking_instance = Booking(user=self.request.user,
            package=self.package_instance,
            number_of_people=self.cleaned_data['number_of_people'],
            booking_date=self.cleaned_data['booking_date'],
            arrival_location=self.cleaned_data['arrival_location'],
            start_date=self.cleaned_data['start_date'],
            additional_info=self.cleaned_data['additional_info'])
        self.booking_instance.save()
        #send emails
        send_mail(fail_silently=fail_silently, **self.get_admin_message_dict())
        # send mail to user
        send_mail(fail_silently=fail_silently,
            recipient_list=[self.booking_instance.user.email],
            **self.get_user_message_dict())
        return self.instance
