from datetime import date
from copy import deepcopy

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from .forms import BookPackageForm
from .forms import MakePackageForm

from .models import Package
from .models import ItemMode
from .models import Booking


def packages(request):
    if request.method == 'POST':
        package_id = request.POST['package_id']
        return HttpResponseRedirect(
            reverse('bookings:book-package', args=(package_id,))
        )
    else:
        packages_query = Package.objects.filter(published=True)
        packages = {}
        for package in packages_query:
            packages[package] = {}
            packages[package]['prices'] = package.get_all_prices()
            packages[package]['included'] = package.items.filter(
                item__included=True)
            packages[package]['excluded'] = package.items.filter(
                item__included=False)
        context = {
            'packages': packages,
        }
        return render(request, 'bookings/packages.html', context)


def package_item(request, item_id):
    item = get_object_or_404(ItemMode, pk=item_id)

    context = {
        'item': item,
    }
    return render(request, 'bookings/item.html', context)


def mybookings(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('auth_login'))

    bookings_query = Booking.objects.filter(user=request.user)
    bookings = {}

    for booking in bookings_query:
        bookings[booking] = {}
        bookings[booking]['package'] = booking.package
        bookings[booking]['included'] = booking.package.items.filter(
            item__included=True)
        bookings[booking]['excluded'] = booking.package.items.filter(
            item__included=False)

    context = {
        'bookings': bookings,
    }
    return render(request, 'bookings/mybookings.html', context)


def book_package(request, package_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('auth_login'))

    package = get_object_or_404(Package, pk=package_id)
    items = {
        'included': package.items.filter(item__included=True),
        'excluded': package.items.filter(item__included=False)
    }
    form = BookPackageForm(request.POST or None, request=request,
        user=request.user, package=package)

    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect(reverse('bookings:booking-completed'))

    context = {
        'package': package,
        'items': items,
        'form': form,
    }

    return render(request, 'bookings/book_package.html', context)


def booking_completed(request):
    return render(request, 'bookings/booking_completed.html')


def create_package(request, package_id=None):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('auth_login'))

    package = None
    items = None
    if package_id:
        package = get_object_or_404(Package, pk=package_id)
        items = package.items.all()

    form = MakePackageForm(request.POST or None, request=request, items=items)
    
    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect(reverse('bookings:booking-completed'))

    context = {
        'form': form,
    }
    return render(request, 'bookings/create_package.html', context)