from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^packages/$', views.packages, name='packages'),
    url(r'^packages/item-(?P<item_id>\d+)/$', views.package_item, name='package-item'),
    url(r'^packages/book-package-(?P<package_id>\d+)/$', views.book_package, name='book-package'),
    url(r'^packages/booking-completed/$', views.booking_completed, name='booking-completed'),
    url(r'^packages/custom-package/$', views.create_package, name='custom-package'),
    url(r'^packages/custom-package/package-(?P<package_id>\d+)/$', views.create_package, name='customize-package'),
    url(r'^mybookings/$', views.mybookings, name='mybookings'),
]
