from modeltranslation.translator import register, TranslationOptions

from .models import Item
from .models import Package
from .models import ArrivalLocation


@register(Item)
class ItemTranslationOptions(TranslationOptions):
    fields = ('title', 'info',)


@register(Package)
class PackageTranslationOptions(TranslationOptions):
	fields = ('name', 'description',)


@register(ArrivalLocation)
class ArrivalLocationTranslationOptions(TranslationOptions):
	fields = ('location',)
