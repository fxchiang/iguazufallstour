from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist

from decimal import Decimal

from tinymce.models import HTMLField


PACKAGE_MODE = (
    ('SH', _('Shared')),
    ('PV', _('Private')),
)

PACKAGE_TYPE = (
    ('custom', 'Custom package'),
    ('offered', 'Offered package')
)

TOURS_COUNTRIES = (
    ('AR', 'Argentina'),
    ('BR', 'Brazil'),
    ('PY', 'Paraguay'),
)

CATEGORIES = (
    ('Hotel', 'Hotel'),
    ('Tour', 'Tour'),
    ('Extra', 'Extra'),
    ('Fee', 'Fee'),
)


class  Country(models.Model):

    name = models.CharField(max_length=255)

    class Meta:
    #     verbose_name = 'País'
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class Item(models.Model):

    published = models.BooleanField(default=True)
    country = models.CharField(max_length=2, choices=TOURS_COUNTRIES, default='AR')
    included = models.BooleanField(default=True)
    title = models.CharField(max_length=255)
    info = HTMLField(blank=True)
    category = models.CharField(max_length=30, choices=CATEGORIES, default='Tour')

    def __str__(self):
        if self.included:
            return self.title
        return self.title + ' not included'

    def get_absolut_url(self):
        return reverse('bookings:package-item', args=(self.pk, ))

    def get_price(self, number_of_people=1):
        price = 0
        try:
            price = ItemPrice.objects.get(
                item=self,
                n_people=number_of_people
            )
            return price.price
        except ObjectDoesNotExist:
            return price


class ItemPrice(models.Model):

    item = models.ForeignKey(Item)
    n_people = models.IntegerField()
    price = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        str = ungettext(
           'For %(count)d person',
           'For %(count)d people',
        self.n_people) % {
           'count': self.n_people,
        }
        return '{} U${}'.format(str, self.price)


class ItemMode(models.Model):

    item = models.ForeignKey(Item)
    mode = models.CharField(max_length=2, choices=PACKAGE_MODE)

    class Meta:
        unique_together =   ('item', 'mode')

    def __str__(self):
        str = self.item.title
        if self.item.included:
            str += ' ({})'.format(self.get_mode_display())
        else:
            str += ' not included'

        return str


class Package(models.Model):

    published = models.BooleanField(default=False)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    items = models.ManyToManyField(
        ItemMode,
        related_name='package_items',
        limit_choices_to=Q(item__category='Tour')|
            Q(item__category='Fee')|
            Q(item__category='Extra')|
            Q(item__category='Hotel')
    )
    profit_percent = models.IntegerField(blank=True, null=True, default=0)
    quoted_price = models.DecimalField(max_digits=19, decimal_places=2,
        blank=True, null=True)
    mode = models.CharField(max_length=2, choices=PACKAGE_MODE, default='PV')
    package_type = models.CharField(
        max_length=30,
        choices=PACKAGE_TYPE,
        default='offered'
    )

    def __str__(self):
        return self.name

    def format_profit(self):
        return str(self.profit_percent)+'%'

    def calculate_cost(self, number_of_people):
        cost = 0
        for item_mode in self.items.filter(item__included=True):
            try:
                item_price = ItemPrice.objects.get(
                    item=item_mode.item,
                    n_people=number_of_people,
                )
                cost += item_price.price
            except ObjectDoesNotExist:
                pass
        return cost

    def calculate_price(self, number_of_people):
        price = self.calculate_cost(number_of_people)
        return round(price * (1 + Decimal(self.profit_percent/100)),2)

    def get_formated_price(self, number_of_people):
        price = self.calculate_price(number_of_people)
        str = ungettext(
           'For %(count)d person',
           'For %(count)d people',
        number_of_people) % {
           'count': number_of_people,
        }
        if self.package_type == 'offered':
            return '{} U${}'.format(str, price)
        elif self.package_type == 'custom' and self.quoted_price:
            return '{} U${}'.format(str, self.quoted_price)
        else:
            return str

    def get_all_prices(self):
        """get avaliable prices for avaliable number of people"""
        show_if_equal = self.items.filter(item__included=True).count()
        prices = {}
        item_prices = []
        for item_mode in self.items.filter(item__included=True):
            item_prices.extend(ItemPrice.objects.filter(
                item=item_mode.item))
        for price in item_prices:
            try:
                prices[price.n_people]
            except KeyError:
                prices[price.n_people] = []
            prices[price.n_people].append(price)
        for n_people in list(prices):
            if len(prices[n_people]) == show_if_equal:
                prices[n_people] = self.get_formated_price(n_people)
            else:
                del prices[n_people]
        return prices


class UserProfile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    address_line_1 = models.CharField(max_length=100)
    address_line_2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.ForeignKey(Country)

    def __str__(self):
        return self.user.email


class ArrivalLocation(models.Model):

    location = models.CharField(max_length=100)

    def __str__(self):
        return self.location


class Booking(models.Model):

    STATUS = (
        ('New', _('New')),
        ('Viewed', _('Viewed')),
        ('Awaiting payment', _('Awaiting payment')),
        ('Confirmed', _('Confirmed')),
        ('Canceled', _('Canceled')),
        ('Closed', _('Closed'))
    )

    status = models.CharField(max_length=50, choices=STATUS, default='New')
    user = models.ForeignKey(User)
    package = models.ForeignKey(Package)
    number_of_people = models.IntegerField(default=1)
    booking_date = models.DateField()
    arrival_location = models.ForeignKey(ArrivalLocation)
    start_date = models.DateField()
    additional_info = models.TextField(null=True, blank=True)

    def user_name(self):
        return self.user.get_name()

    def user_email(self):
        return str(self.user.email)
    user_email.short_description = 'Email'

    def get_formated_price(self):
        return self.package.get_formated_price(self.number_of_people)

    def get_price(self):
        return self.package.calculate_price(self.number_of_people)
