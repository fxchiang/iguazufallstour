from django.contrib import admin

from modeltranslation.admin import TranslationAdmin

from .models import UserProfile
from .models import Package
from .models import ItemPrice
from .models import Item
from .models import ItemMode
from .models import Country
from .models import Booking
from .models import ArrivalLocation

from .forms import PackageModelForm
from .forms import ItemModeInlineFormset


# Register your models here.
class CountryAdmin(admin.ModelAdmin):
    ordering = ['name']


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('get_user_email',
        'get_user_first_name',
        'get_user_last_name',
        'country',
        'city',
        'state'
    )
    list_filter = ('country', 'state', 'city')
    search_fields = [
        'country',
        'city',
        'state',
        'address_line_1',
        'address_line_2'
    ]
    fieldsets = [
        ('Customer information', {'fields': ['user']}),
        ('Address', {'fields': [
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'country'
        ]})
    ]

    def get_user_email(self, obj):
        return obj.user.email
    get_user_email.short_description = 'Email'

    def get_user_first_name(self, obj):
        return obj.user.first_name
    get_user_first_name.short_description = 'First Name'

    def get_user_last_name(self, obj):
        return obj.user.last_name
    get_user_last_name.short_description = 'Last Name'


class PriceInLine(admin.TabularInline):
    model = ItemPrice
    extra = 0

class ItemModeInLine(admin.TabularInline):
    model = ItemMode
    formset = ItemModeInlineFormset
    max_num = 2
    extra = 0


class ItemAdmin(TranslationAdmin):
    list_display = (
        'title',
        'category',
        'published',
        'included',
        'country',
    )

    list_filter = ('country', 'category', 'included')
    search_fields = ['title', 'info']

    fieldsets = [
        ('Item information', {'fields': [
            'included',
            'category',
            'country',
            'title',
            'info'
        ]}),

    ]

    inlines = [ItemModeInLine, PriceInLine]


class PackageAdmin(TranslationAdmin):
    form = PackageModelForm
    list_display = (
        'name',
        'mode',
        'package_type',
        'profit',
        'quoted_price',
        'published',
    )
    list_filter = ('mode', 'package_type')
    filter_horizontal = ('items',)
    search_fields = ['name', 'description']
    fieldsets = [
        ('Content information', {'fields': ['published']}),
        ('Package information', {'fields': [
            'mode',
            'package_type',
            'name',
            'description'
        ]}),
        ('Package items', {'fields': ['items']}),
        ('Pricing', {'fields': ['profit_percent', 'quoted_price']}),
    ]

    def price(self, obj):
        return obj.calculate_price()

    def cost(self, obj):
        return obj.calculate_cost()

    def profit(self, obj):
        return obj.format_profit()


class ArrivalLocationAdmin(TranslationAdmin):
    pass


class BookingAdmin(admin.ModelAdmin):
    list_display = ('get_user_email',
        'package',
        'status',
        'booking_date',
        'start_date'
    )

    def get_user_email(self, obj):
        return obj.user.email
    get_user_email.short_description = 'Email'


admin.site.register(Package, PackageAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Booking, BookingAdmin)
admin.site.register(ArrivalLocation, ArrivalLocationAdmin)