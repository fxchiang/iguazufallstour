# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0009_auto_20160311_0337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='name',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='package',
            name='name_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='package',
            name='name_pt_br',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
