# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0007_auto_20160304_1245'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='package',
            name='hotel',
        ),
    ]
