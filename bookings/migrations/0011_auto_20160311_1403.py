# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0010_auto_20160311_1259'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='aditional_info',
            new_name='additional_info',
        ),
    ]
