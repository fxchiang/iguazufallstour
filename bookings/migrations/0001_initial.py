# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ArrivalLocation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('location', models.CharField(max_length=100)),
                ('location_en', models.CharField(max_length=100, null=True)),
                ('location_pt_br', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('status', models.CharField(max_length=50, choices=[('New', 'New'), ('Viewed', 'Viewed'), ('Awaiting payment', 'Awaiting payment'), ('Confirmed', 'Confirmed'), ('Canceled', 'Canceled'), ('Closed', 'Closed')], default='New')),
                ('number_of_people', models.IntegerField(default=1)),
                ('booking_date', models.DateField()),
                ('start_date', models.DateField()),
                ('arrival_time', models.TimeField(blank=True, null=True)),
                ('aditional_info', models.TextField(blank=True, null=True)),
                ('arrival_location', models.ForeignKey(to='bookings.ArrivalLocation')),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('published', models.BooleanField(default=True)),
                ('country', models.CharField(max_length=2, choices=[('AR', 'Argentina'), ('BR', 'Brazil'), ('PY', 'Paraguay')], default='AR')),
                ('included', models.BooleanField(default=True)),
                ('title', models.CharField(max_length=255)),
                ('title_en', models.CharField(max_length=255, null=True)),
                ('title_pt_br', models.CharField(max_length=255, null=True)),
                ('info', tinymce.models.HTMLField(blank=True)),
                ('info_en', tinymce.models.HTMLField(blank=True, null=True)),
                ('info_pt_br', tinymce.models.HTMLField(blank=True, null=True)),
                ('category', models.CharField(max_length=30, choices=[('Hotel', 'Hotel'), ('Tour', 'Tour'), ('Extra', 'Extra'), ('Fee', 'Fee')], default='Tour')),
            ],
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('published', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255, default='Custom package')),
                ('name_en', models.CharField(max_length=255, null=True, default='Custom package')),
                ('name_pt_br', models.CharField(max_length=255, null=True, default='Custom package')),
                ('description', models.TextField(blank=True, null=True)),
                ('description_en', models.TextField(blank=True, null=True)),
                ('description_pt_br', models.TextField(blank=True, null=True)),
                ('profit_percent', models.IntegerField(blank=True, null=True, default=0)),
                ('mode', models.CharField(max_length=2, choices=[('SH', 'Shared'), ('PV', 'Private')], default='PV')),
                ('package_type', models.CharField(max_length=30, choices=[('custom', 'Custom package'), ('special', 'Special tour'), ('offered', 'Offered package')], default='offered')),
            ],
        ),
        migrations.CreateModel(
            name='PackageItemCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Package item categories',
            },
        ),
        migrations.CreateModel(
            name='PackageItemMode',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('mode', models.CharField(max_length=2, choices=[('SH', 'Shared'), ('PV', 'Private')])),
                ('item', models.ForeignKey(to='bookings.Item')),
            ],
        ),
        migrations.CreateModel(
            name='PackageItemPrice',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('n_people', models.IntegerField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('package_item', models.ForeignKey(to='bookings.Item')),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('address_line_1', models.CharField(max_length=100)),
                ('address_line_2', models.CharField(blank=True, max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('country', models.ForeignKey(to='bookings.Country')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='package',
            name='hotel',
            field=models.ForeignKey(to='bookings.PackageItemMode', related_name='package_hotel'),
        ),
        migrations.AddField(
            model_name='package',
            name='items',
            field=models.ManyToManyField(related_name='package_items', to='bookings.PackageItemMode'),
        ),
        migrations.AddField(
            model_name='booking',
            name='package',
            field=models.ForeignKey(to='bookings.Package'),
        ),
        migrations.AddField(
            model_name='booking',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
