# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0004_auto_20160228_1656'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='arrival_time',
        ),
    ]
