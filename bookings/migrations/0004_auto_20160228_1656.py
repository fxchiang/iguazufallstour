# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0003_auto_20160228_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='items',
            field=models.ManyToManyField(to='bookings.PackageItemMode', related_name='package_items'),
        ),
    ]
