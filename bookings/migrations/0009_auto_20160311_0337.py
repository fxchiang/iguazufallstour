# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0008_remove_package_hotel'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='quoted_price',
            field=models.DecimalField(max_digits=19, blank=True, decimal_places=2, null=True),
        ),
        migrations.AlterField(
            model_name='package',
            name='package_type',
            field=models.CharField(choices=[('custom', 'Custom package'), ('offered', 'Offered package')], default='offered', max_length=30),
        ),
    ]
