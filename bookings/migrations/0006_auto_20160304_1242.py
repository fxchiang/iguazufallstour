# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0005_remove_booking_arrival_time'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemMode',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('mode', models.CharField(choices=[('SH', 'Shared'), ('PV', 'Private')], max_length=2)),
                ('item', models.ForeignKey(to='bookings.Item')),
            ],
        ),
        migrations.CreateModel(
            name='ItemPrice',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('n_people', models.IntegerField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('item', models.ForeignKey(to='bookings.Item')),
            ],
        ),
        migrations.DeleteModel(
            name='PackageItemCategory',
        ),
        migrations.RemoveField(
            model_name='packageitemmode',
            name='item',
        ),
        migrations.RemoveField(
            model_name='packageitemprice',
            name='item',
        ),
        migrations.AlterField(
            model_name='package',
            name='hotel',
            field=models.ForeignKey(related_name='package_hotel', to='bookings.ItemMode'),
        ),
        migrations.AlterField(
            model_name='package',
            name='items',
            field=models.ManyToManyField(related_name='package_items', to='bookings.ItemMode'),
        ),
        migrations.DeleteModel(
            name='PackageItemMode',
        ),
        migrations.DeleteModel(
            name='PackageItemPrice',
        ),
    ]
