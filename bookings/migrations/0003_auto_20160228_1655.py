# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0002_auto_20160228_0609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='items',
            field=models.ManyToManyField(related_name='items', to='bookings.PackageItemMode'),
        ),
    ]
